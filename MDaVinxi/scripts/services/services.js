﻿(function () {
    var app = angular.module('mDavinxi');

    app.service('svc', ['$http',function ($http) {
        var svc = {};
        svc.showAlert = function (titleDialog, alertMessage) {
            ons.notification.alert({
                title: titleDialog,
                message: alertMessage
            });
        }

        svc.isAuth = function () {
            return svc.getAuth() != null;
        }

        svc.getAuth = function () {
            return localStorage.getItem("davinxi-login");
        }

        svc.setAuth = function (data) {
            return localStorage.setItem("davinxi-login", data);
        }

        svc.removeAuth = function () {
            localStorage.removeItem('davinxi-login');
        }

        svc.APIServer = function () {
            if (localStorage.getItem("davinxi-api-server") == null)
                localStorage.setItem("davinxi-api-server", "http://localhost:3032");
            return localStorage.getItem("davinxi-api-server");
        }

        svc.GetCompany = function (companyID) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetCompaniesJSON",
                        { params: { company_id: companyID } });
            }
        }

        svc.GetDivision = function (company_id) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetDivisionsJSON",
                        { params: { companyID: company_id } });
            }
        }

        svc.GetBlock = function (company_id, division_id) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetBlocksJSON",
                        { params: { companyID: company_id, divisionID: division_id } });
            }
        }

        svc.GetMachine = function (company_id, division_id, block_id)
        {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetMachinesJSON",
                        { params: { companyID: company_id, divisionID: division_id, blockID: block_id } });
            }
        }

        svc.GetSKUCategory = function (company_id) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetSKUCategoriesJSON",
                        { params: { companyID: company_id } });
            }
        }

        svc.GetSKUSubCategory = function (company_id, category_id) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetSKUSubCategoriesJSON",
                        { params: { companyID: company_id, categoryID: category_id } });
            }
        }

        svc.GetSKUSegment = function (company_id) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetSKUSegmentsJSON",
                        { params: { companyID: company_id } });
            }
        }

        svc.GetSKUBusiness = function (company_id) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetSKUBusinessJSON",
                        { params: { companyID: company_id } });
            }
        }

        svc.GetSKU = function (company_id, category_id, subcategory_id, segment_id, business_id) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetSKUJSON",
                        {
                            params: {
                                companyID: company_id,
                                categoryID: category_id,
                                subcategoryID: subcategory_id,
                                segmentID: segment_id,
                                businessID: business_id
                            }
                        });
            }
        }

        svc.GetQualityProperties = function (company_id, sku_id) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetPropertySettingJSON",
                        { params: { companyID: company_id, skuID: sku_id } });
            }
        }

        svc.GetBatch = function (company_id, sku_id) {
            var LoginInfo = JSON.parse(svc.getAuth());
            if (LoginInfo != null) {
                return $http.get(svc.APIServer() + "/HelperClass/GetBatchesJSON",
                        { params: { companyID: company_id, skuID: sku_id } });
            }
        }

        return svc;
    }]);
})();