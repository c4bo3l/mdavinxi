﻿(function () {
    var app = angular.module('mDavinxi');

    app.service('qData', function () {
        var svc = {}, qData = null;
        svc.SetData = function (data) {
            qData = data;
        }

        svc.GetData = function () {
            return qData;
        }

        return svc;
    });
})();