﻿(function () {
    var app = angular.module('mDavinxi');

    app.controller("DowntimeTrackingController",
        ['$scope', 'svc', '$q', '$http', 'qData', '$filter', '$cordovaDatePicker',
        function ($scope, svc, $q, $http, qData, $filter, $cordovaDatePicker) {
            var LoginInfo = JSON.parse(svc.getAuth()),
                StartDate = new Date("Oct 1 2014 00:00"), FinishDate = new Date();
            $scope.Params = {
                company_id: LoginInfo.company_id,
                division_id: '',
                block_id: '',
                machine_id: '',
                targetParent: '',
                start_date: '',
                finish_date: ''
            };

            $scope.Severity = [
                {
                    id: 0,
                    text: 'No defect'
                },
                {
                    id: 1,
                    text: 'Just noticeable'
                },
                {
                    id: 2,
                    text: 'Warning'
                },
                {
                    id: 3,
                    text: 'Downgrade'
                },
                {
                    id: 4,
                    text: 'Reject'
                }
            ];

            $scope.TimeType = [
                {
                    id: 0,
                    text: 'Hour(s)'
                },
                {
                    id: 1,
                    text: 'Day(s)'
                },
                {
                    id: 2,
                    text: 'Week(s)'
                },
                {
                    id: 3,
                    text: 'Month(s)'
                },
                {
                    id: 4,
                    text: 'Year(s)'
                }
            ];

            $scope.showDateTimePicker = function (dateType) {
                var DateTime = dateType == 0 ? StartDate : FinishDate;
                var options = { date: DateTime, mode: 'date' };
                $cordovaDatePicker.show(options).then(function (date) {
                    if (date != null) {
                        DateTime.setFullYear(date.getFullYear());
                        DateTime.setMonth(date.getMonth());
                        DateTime.setDate(date.getDate());

                        options = { date: DateTime, mode: 'time' };
                        $cordovaDatePicker.show(options).then(function (date) {
                            if (date != null) {
                                DateTime.setHours(date.getHours());
                                DateTime.setMinutes(date.getMinutes());
                                if (dateType == 0)
                                    StartDate = DateTime;
                                else
                                    FinishDate = DateTime;
                                $scope.refreshDate();
                            }
                        });
                    }
                });
            }

            $scope.refreshDate = function () {
                $scope.Params.start_date = $filter('date')(StartDate, 'MMM dd yyyy HH:mm');
                $scope.Params.finish_date = $filter('date')(FinishDate, 'MMM dd yyyy HH:mm');
            }

            $scope.selectedText = {
                property: 'All'
            };

            $scope.CB = {};
            $scope.modalText = "Retrieving required data...";
            angular.element(document).ready(function () {
                if (qData.GetData() == null) {
                    $scope.openModal();
                    $scope.GetCompany().then(function () {
                        $scope.onCompanyChange();
                    });
                    StartDate.setDate(StartDate.getDate() - 1);
                    $scope.refreshDate();
                }
            });

            $scope.isModalShowed = false;
            $scope.openModal = function () {
                if (!$scope.isModalShowed) {
                    modal.show();
                    $scope.isModalShowed = true;
                }
            }

            $scope.closeModal = function () {
                if ($scope.isModalShowed) {
                    modal.hide();
                    $scope.isModalShowed = false;
                }
            }

            $scope.GetCompany = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving companies...";
                svc.GetCompany($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Companies = data;
                        if (data.length > 0)
                            $scope.Params.company_id = data[0].company_id;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Companies = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onCompanyChange = function () {
                $scope.openModal();
                $scope.GetDivision().then(function () {
                    $scope.GetBlock().then
                    (
                        function () {
                            $scope.GetBlock().then
                                (
                                    function () {
                                        $scope.onBlockChange();
                                    }
                                );
                        }
                    );
                });
            }

            $scope.GetDivision = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving divisions...";
                $scope.Params.division_id = '';
                svc.GetDivision($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Divisions = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Divisions = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onDivisionChange = function () {
                $scope.openModal();
                $scope.GetBlock().then
                    (
                        function () {
                            $scope.GetBlock().then
                                (
                                    function () {
                                        $scope.onBlockChange();
                                    }
                                );
                        }
                    );
            }

            $scope.GetBlock = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving blocks...";
                $scope.Params.block_id = '';
                svc.GetBlock($scope.Params.company_id, $scope.Params.division_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Blocks = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Blocks = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onBlockChange = function () {
                $scope.openModal();
                var promise = $scope.GetMachine();
                promise.then(function () { $scope.closeModal(); });
                return promise;
            }

            $scope.GetMachine = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving machines...";
                $scope.Params.machine_id = '';
                svc.GetMachine($scope.Params.company_id, $scope.Params.division_id, $scope.Params.block_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Machines = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Machines = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.chartConfig = qData.GetData();
            if ($scope.chartConfig != null && $scope.chartConfig.Params != null)
                $scope.Params = $scope.chartConfig.Params;


            $scope.Process = function () {
                $scope.chartConfig = {};
                $scope.chartConfig.Breadcrumbs = [];
                qData.SetData($scope.chartConfig);
                $scope.Params.targetParent = '';
                $scope.modalText = 'Retrieving downtime tracking data...';
                $scope.openModal();
                $scope.GetChart().then(function () {
                    $scope.closeModal();
                    myNav.pushPage('views/Analysis/downtime_tracking/downtime_tracking_result.html');
                }, function () {
                    svc.showAlert("Result Confirmation", "No data");
                    $scope.closeModal();
                });
            }
            
            $scope.GetChart = function () {
                var q = $q.defer();
                $http.get(svc.APIServer() + "/DowntimeTracking/GetDowntimeTracking",
                            { params: $scope.Params }).
                    success(function (data, status, headers, config) {
                        $scope.modalText = 'Generating chart...';
                        if (data != null && data.dAction != null && data.dAction.length > 0) {
                            $scope.InitActionChart(data.dAction);
                            if (data != null && data.tracking != null && data.tracking.length > 0) {
                                $scope.InitFreqPareto(data.tracking);
                                $scope.InitDurationPareto(data.tracking);
                            }
                            $scope.chartConfig.Params = $scope.Params;
                            qData.SetData($scope.chartConfig);
                            q.resolve();
                        }
                        else
                            q.reject();
                    }).
                    error(function (data, status, headers, config) {
                        q.reject();
                        $scope.closeModal();
                        svc.showAlert('Error Occured', 'Failed to draw defect tracking chart');
                    });
                return q.promise;
            }

            $scope.InitActionChart = function (data) {
                if (data != null && data.length > 0) {
                    data.sort
                            (
                                function (a, b) {
                                    return Number(b.count) - Number(a.count);
                                }
                            );
                    SumCount = 0;
                    for (var i = 0; i < data.length; i++) {
                        SumCount += Number(data[i].count);
                    }

                    var Percent = [], Points = [];
                    TempCount = 0;
                    for (var i = 0; i < data.length; i++) {
                        Points.push(data[i].count)
                        TempCount += Number(data[i].count);
                        Percent.push((TempCount / SumCount) * 100);
                    }
                    $scope.chartConfig.actionChart = $scope.InitPareto(data, Points, Percent, "Count",
                        "Downtime Solution(s)", true, false, true);
                }
            }

            $scope.InitPareto = function (data, points, percentage, variableText, title, isCount, allowClick, isAction) {
                return {
                    options: {
                        chart: {
                            type: 'column',
                            zoomType: 'xy',
                            height: 450
                        },
                        tooltip: {
                            headerFormat: (isAction ? '<span style="font-size: 10px">{point.key.action_name}</span><br/>' : '<span style="font-size: 10px">{point.key.downtime_name}</span><br/>'),
                            shared: true
                        },
                        plotOptions: {
                            column: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function () {
                                            if (allowClick) {
                                                var Downtime = isCount ? $scope.FreqData : $scope.DurationData;
                                                if ($scope.chartConfig != null && $scope.chartConfig.Breadcrumbs != null &&
                                                    ($scope.chartConfig.Breadcrumbs.length <= 0 ||
                                                        $scope.chartConfig.Breadcrumbs[$scope.chartConfig.Breadcrumbs.length - 1].downtime_id !=
                                                        Downtime[this.x].downtime_id)) {
                                                    $scope.modalText = "Retrieving " + Downtime[this.x].downtime_name + " data...";
                                                    result_modal.show();
                                                    $scope.chartConfig.Breadcrumbs.push(Downtime[this.x]);
                                                    $scope.Params.targetParent = Downtime[this.x].downtime_id;
                                                    $scope.GetChart().then(function () {
                                                        tabbar.setActiveTab(tabbar.getActiveTabIndex());
                                                        result_modal.hide();
                                                    });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    title: {
                        text: title
                    },
                    subtitle: {
                        text: "Downtime Category : " + ($scope.chartConfig.Breadcrumbs.length <= 0 ? "All" :
                                $scope.chartConfig.Breadcrumbs[$scope.chartConfig.Breadcrumbs.length - 1].downtime_name)
                    },
                    xAxis: {
                        categories: data,
                        labels: {
                            formatter: function () {
                                return isAction ? this.value.action_name : this.value.downtime_name;
                            },
                            rotation: 90
                        }
                    },
                    yAxis: [
                        {
                            gridLineWidth: 0,
                            title: {
                                text: "Percentage",
                                style: {
                                    color: '#4572A7'
                                }
                            },
                            labels: {
                                formatter: function () {
                                    return this.value + ' %';
                                }
                            },
                            opposite: true,
                            tickInterval: 10,
                            min: 0,
                            max: 100
                        },
					    {
					        title: {
					            text: isCount ? "Frequency" : "Duration (minute)",
					            style: {
					                color: '#89A54E'
					            }
					        }
					    }
                    ],
                    series: [
                        {
                            name: isCount ? "Frequency" : "Duration",
                            type: 'column',
                            yAxis: 1,
                            data: points
                        },
                        {
                            name: "Percentage",
                            type: 'spline',
                            data: percentage,
                            tooltip: {
                                valueSuffix: ' %'
                            }
                        }
                    ]
                };
            }

            $scope.FreqData = [];
            $scope.InitFreqPareto = function (data) {
                if (data != null && data.length > 0) {
                    data.sort
                        (
                            function (a, b) {
                                return Number(b.count) - Number(a.count);
                            }
                        );
                    $scope.FreqData = data;
                    var Sum = 0;
                    for (var i = 0; i < data.length; i++) {
                        Sum += Number(data[i].count);
                    }

                    var Percent = [], Points = [], CurrentSum = 0;
                    for (var i = 0; i < data.length; i++) {
                        Points.push(Number(data[i].count));
                        CurrentSum += Number(data[i].count);
                        Percent.push((CurrentSum / Sum) * 100.0);
                    }

                    $scope.chartConfig.freqChart = $scope.InitPareto(data, Points, Percent, "Frequency",
                        "Downtime Tracking", true, true, false);
                }
            }

            $scope.DurationData = [];
            $scope.InitDurationPareto = function (data) {
                if (data != null && data.length > 0) {
                    data.sort
                        (
                            function (a, b) {
                                return Number(b.sum_duration) - Number(a.sum_duration);
                            }
                        );
                    $scope.DurationData = data;
                    var Sum = 0;
                    for (var i = 0; i < data.length; i++) {
                        Sum += Number(data[i].sum_duration);
                    }

                    var Percent = [], Points = [], CurrentSum = 0;
                    for (var i = 0; i < data.length; i++) {
                        Points.push(Number(data[i].sum_duration));
                        CurrentSum += Number(data[i].sum_duration);
                        Percent.push((CurrentSum / Sum) * 100.0);
                    }

                    $scope.chartConfig.durationChart = $scope.InitPareto(data, Points, Percent, "Duration",
                        "Downtime Tracking", false, true, false);
                }
            }

            $scope.back = function () {
                if ($scope.chartConfig.Breadcrumbs.length <= 0)
                    myNav.popPage();
                else {
                    $scope.chartConfig.Breadcrumbs.pop();
                    var DowntimeID = '';
                    if ($scope.chartConfig.Breadcrumbs.length > 0)
                        DowntimeID = $scope.chartConfig.Breadcrumbs[$scope.chartConfig.Breadcrumbs.length - 1].downtime_id;
                    $scope.chartConfig.Params.targetParent = DowntimeID;
                    $scope.Params = $scope.chartConfig.Params;
                    $scope.modalText = "Retrieving previous data...";
                    result_modal.show();
                    $scope.GetChart().then(function () { tabbar.setActiveTab(tabbar.getActiveTabIndex()); result_modal.hide(); });
                }
            }
        }]);
})();