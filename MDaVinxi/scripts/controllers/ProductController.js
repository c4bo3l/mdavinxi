﻿(function () {
    var app = angular.module('mDavinxi');

    app.controller("ProductController", ['$scope', 'svc', 'qData', '$q', '$http', '$cordovaBarcodeScanner',
        function ($scope, svc, qData, $q, $http, $cordovaBarcodeScanner) {
        var LoginInfo = JSON.parse(svc.getAuth());
        $scope.Params = {
            company_id: LoginInfo.company_id,
            division_id: '',
            block_id: '',
            machine_id: '',
            category_id: '',
            subcategory_id: '',
            business_id: '',
            segment_id: '',
            sku_id: '',
            batch_id: '',
            product_id: ''
        };

        $scope.modalText = '';
        $scope.CB = {};
        $scope.modalText = "Retrieving required data...";
        angular.element(document).ready(function () {
            if (qData.GetData() == null) {
                $scope.openModal();
                $scope.GetCompany().then(function () {
                    $scope.onCompanyChange();
                });
            }
        });

        $scope.isModalShowed = false;
        $scope.openModal = function () {
            if (!$scope.isModalShowed) {
                if (modal != null)
                    modal.show();
                else
                    result_modal.show();
                $scope.isModalShowed = true;
            }
        }

        $scope.closeModal = function () {
            if ($scope.isModalShowed) {
                if (modal != null)
                    modal.hide();
                else
                    result_modal.hide();
                $scope.isModalShowed = false;
            }
        }

        $scope.GetCompany = function () {
            var q = $q.defer();
            $scope.modalText = "Retrieving companies...";
            svc.GetCompany($scope.Params.company_id).
                success(function (data, status, headers, config) {
                    $scope.CB.Companies = data;
                    if (data.length > 0)
                        $scope.Params.company_id = data[0].company_id;
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.Companies = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.onCompanyChange = function () {
            $scope.openModal();
            $scope.GetDivision().then(function () {
                $scope.GetBlock().then
                (
                    function () {
                        $scope.GetBlock().then
                            (
                                function () {
                                    $scope.onBlockChange();
                                }
                            );
                    }
                ).then(function () {
                    $scope.GetSKUSegment().then(function () {
                        $scope.GetSKUBusiness().then(function () {
                            $scope.GetSKUCategory().then(function () {
                                $scope.onSKUCategoryChange();
                            });
                        });
                    });
                });
            });
        }

        $scope.GetDivision = function () {
            var q = $q.defer();
            $scope.modalText = "Retrieving divisions...";
            $scope.Params.division_id = '';
            svc.GetDivision($scope.Params.company_id).
                success(function (data, status, headers, config) {
                    $scope.CB.Divisions = data;
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.Divisions = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.onDivisionChange = function () {
            $scope.openModal();
            $scope.GetBlock().then
                (
                    function () {
                        $scope.GetBlock().then
                            (
                                function () {
                                    $scope.onBlockChange();
                                }
                            );
                    }
                );
        }

        $scope.GetBlock = function () {
            var q = $q.defer();
            $scope.modalText = "Retrieving blocks...";
            $scope.Params.block_id = '';
            svc.GetBlock($scope.Params.company_id, $scope.Params.division_id).
                success(function (data, status, headers, config) {
                    $scope.CB.Blocks = data;
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.Blocks = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.onBlockChange = function () {
            $scope.openModal();
            var promise = $scope.GetMachine();
            promise.then(function () { $scope.closeModal(); });
            return promise;
        }

        $scope.GetMachine = function () {
            var q = $q.defer();
            $scope.modalText = "Retrieving machines...";
            $scope.Params.machine_id = '';
            svc.GetMachine($scope.Params.company_id, $scope.Params.division_id, $scope.Params.block_id).
                success(function (data, status, headers, config) {
                    $scope.CB.Machines = data;
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.Machines = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.GetSKUCategory = function () {
            var q = $q.defer();
            $scope.modalText = "Retrieving SKU categories...";
            $scope.Params.category_id = '';
            svc.GetSKUCategory($scope.Params.company_id).
                success(function (data, status, headers, config) {
                    $scope.CB.Categories = data;
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.Categories = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.onSKUCategoryChange = function () {
            $scope.Params.subcategory_id = '';

            $scope.openModal();
            $scope.GetSKUSubCategory().then
                (
                    function () {
                        $scope.onSKUSubCategoryChange();
                    }
                );
        }

        $scope.GetSKUSubCategory = function () {
            var q = $q.defer();
            $scope.modalText = "Retrieving SKU subcategories...";
            $scope.Params.subcategory_id = '';
            svc.GetSKUSubCategory($scope.Params.company_id, $scope.Params.category_id).
                success(function (data, status, headers, config) {
                    $scope.CB.SubCategories = data;
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.SubCategories = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.onSKUSubCategoryChange = function () {
            $scope.Params.sku_id = '';
            $scope.openModal();
            $scope.GetSKU().then
                (
                    function () {
                        $scope.closeModal();
                    }
                );
        }

        $scope.GetSKUSegment = function () {
            var q = $q.defer();
            $scope.modalText = "Retrieving SKU segments...";
            $scope.Params.segment_id = '';
            svc.GetSKUSegment($scope.Params.company_id).
                success(function (data, status, headers, config) {
                    $scope.CB.Segments = data;
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.Segments = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.GetSKUBusiness = function () {
            var q = $q.defer();
            $scope.modalText = "Retrieving SKU businesses...";
            $scope.Params.business_id = '';
            svc.GetSKUBusiness($scope.Params.company_id).
                success(function (data, status, headers, config) {
                    $scope.CB.Business = data;
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.Business = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.GetSKU = function () {
            var q = $q.defer();
            $scope.Params.sku_id = '';
            $scope.modalText = "Retrieving SKUs...";
            $scope.CB.Batches = {};
            $scope.CB.Properties = {};
            svc.GetSKU($scope.Params.company_id, $scope.Params.category_id, $scope.Params.subcategory_id,
                $scope.Params.segment_id, $scope.Params.business_id).
                success(function (data, status, headers, config) {
                    $scope.CB.SKUs = data;
                    if (data.length > 0) {
                        $scope.onSKUChange(data[0]);
                    }
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.SKUs = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.onSKUChange = function () {
            var Text = '';
            if ($scope.CB.SKUs != null && $scope.CB.SKUs.length > 0) {
                for (var i = 0; i < $scope.CB.SKUs.length; i++) {
                    if ($scope.CB.SKUs[i].sku_id == $scope.Params.sku_id) {
                        Text = $scope.CB.SKUs[i].sku_name;
                        break;
                    }
                }
            }
            $scope.Params.batch_id = '';

            $scope.openModal();
            $scope.GetBatch().then(function () {
                $scope.closeModal();
            });
        }

        $scope.GetBatch = function () {
            var q = $q.defer();
            $scope.Params.batch_id = '';
            $scope.modalText = "Retrieving batches...";
            svc.GetBatch($scope.Params.company_id, $scope.Params.sku_id).
                success(function (data, status, headers, config) {
                    $scope.CB.Batches = data;
                    q.resolve();
                }).
                error(function (data, status, headers, config) {
                    $scope.CB.Batches = {};
                    q.reject();
                });
            return q.promise;
        }

        $scope.Products = qData.GetData();
        $scope.ProductLazyRepeat = {
            configureItemScope: function (index, itemScope) {
                itemScope.item = null;
                if ($scope.Products != null) {
                    itemScope.item = $scope.Products.data[index];
                }
            },
            calculateItemHeight: function (index) {
                return 140;
            },
            countItems: function () {
                return $scope.Products == null ? 0 : $scope.Products.data.length;
            }
        };

        $scope.QualityLazyRepeat = {
            configureItemScope: function (index, itemScope) {
                itemScope.item = null;
                if ($scope.Products != null && $scope.Products.quality_data != null &&
                    $scope.Products.quality_data.length > 0) {
                    itemScope.item = $scope.Products.quality_data[index];
                }
            },
            calculateItemHeight: function (index) {
                return 90;
            },
            countItems: function () {
                return $scope.Products != null &&
                    $scope.Products.quality_data != null ?
                    $scope.Products.quality_data.length : 0;
            }
        };

        $scope.RejectLazyRepeat = {
            configureItemScope: function (index, itemScope) {
                itemScope.item = null;
                if ($scope.Products != null && $scope.Products.defect != null &&
                    $scope.Products.defect.length > 0) {
                    itemScope.item = $scope.Products.defect[index];
                }
            },
            calculateItemHeight: function (index) {
                return 90;
            },
            countItems: function () {
                return $scope.Products != null &&
                    $scope.Products.defect != null ?
                    $scope.Products.defect.length : 0;
            }
        };

        $scope.FindProduct = function () {
            $scope.modalText = "Searching product...";
            $scope.Products = {};
            modal.show();
            $http.get(svc.APIServer() + "/HelperClass/FindProduct", { params: $scope.Params }).
              success(function (data, status, headers, config) {
                  if (data != null && data.length > 0) {
                      $scope.Products = {};
                      $scope.Products.data = data;
                      $scope.Products.selectedIndex = -1;
                      qData.SetData($scope.Products);
                      myNav.pushPage('views/Production/search_product.html');
                      modal.hide();
                  }
                  else {
                      modal.hide();
                      svc.showAlert("Product Result", "No data found");
                  }
              }).
              error(function (data, status, headers, config) {
                  modal.hide();
                  svc.showAlert("Error Occured", "Failed to retrieve product");
              });
        }

        $scope.SelectProduct = function (index) {
            if ($scope.Products != null) {
                $scope.Products.selectedIndex = index;
                qData.SetData($scope.Products);
                result_modal.show();
                var CompanyID = $scope.Products.data[index].company_id, ProductID = $scope.Products.data[index].product_id;
                $scope.GetQualityProduct(CompanyID, ProductID).then(function () {
                    $scope.GetDefect(CompanyID, ProductID).then(function () {
                        result_modal.hide();
                        myNav.pushPage("views/Production/quality_data_detail.html");
                    });
                });
            }
        }

        $scope.GetQualityProduct = function (companyID, productID) {
            var q = $q.defer();
            $scope.Products.quality_data = null;
            $http.get(svc.APIServer() + "/HelperClass/GetQualityData",
                {
                    params: {
                        company_id: companyID,
                        product_id: productID
                    }
                }).
              success(function (data, status, headers, config) {
                  if (data != null && data.length > 0) {
                      $scope.Products.quality_data = data;
                      qData.SetData($scope.Products);
                      q.resolve();
                  }
                  else {
                      q.reject();
                      svc.showAlert("Product Result", "No quality Data");
                  }
              }).
              error(function (data, status, headers, config) {
                  q.reject();
                  svc.showAlert("Error Occured", "Failed to retrieve quality information");
              });

            return q.promise;
        }

        $scope.GetDefect = function (companyID, productID) {
            var q = $q.defer();
            $scope.Products.defect = null;
            $http.get(svc.APIServer() + "/HelperClass/GetProductDefect",
                {
                    params: {
                        company_id: companyID,
                        product_id: productID
                    }
                }).
              success(function (data, status, headers, config) {
                  if (data != null && data.length > 0) {
                      $scope.Products.defect = data;
                      qData.SetData($scope.Products);
                  }
                  q.resolve();
              }).
              error(function (data, status, headers, config) {
                  q.reject();
                  svc.showAlert("Error Occured", "Failed to retrieve quality information");
              });
            return q.promise;
        }

        $scope.barcodeScan = function () {
            $cordovaBarcodeScanner.scan().then
                (
                    function (result) {
                        var JSONString = JSON.parse(result.text);
                        if (JSONString.company_id != null && JSONString.product_id != null) {
                            if ($scope.Params.company_id <= 0 ||
                                $scope.Params.company_id == Number(JSONString.company_id)) {
                                $scope.Params.company_id = Number(JSONString.company_id);
                                $scope.Params.division_id = '';
                                $scope.Params.block_id = '';
                                $scope.Params.machine_id = '';
                                $scope.Params.category_id = '';
                                $scope.Params.subcategory_id = '';
                                $scope.Params.segment_id = '';
                                $scope.Params.business_id = '';
                                $scope.Params.sku_id = '';
                                $scope.Params.batch_id = '';
                                $scope.Params.product_id = JSONString.product_id;
                                $scope.FindProduct();
                            }
                            else
                                svc.showAlert('Error Occured', "You're not allowed to get this data");
                        }
                    },
                    function (err) {
                        svc.showAlert('Error occured', err);
                    }
                );
        }
    }]);
})();