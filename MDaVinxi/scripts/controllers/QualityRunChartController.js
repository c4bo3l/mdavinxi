﻿(function () {
    var app = angular.module('mDavinxi');

    app.controller("QualityRunChartController",
        ['$scope', 'svc', '$q', '$http', 'qData', '$filter',
        function ($scope, svc, $q, $http, qData, $filter) {
            var LoginInfo = JSON.parse(svc.getAuth()),
                StartDate = new Date(), FinishDate = new Date();
            $scope.Params = {
                company_id: LoginInfo.company_id,
                machine_id: '',
                sku_id: '',
                batch_id: '',
                property_id: '',
                we_rules: [
                    {
                        id: 'WE1',
                        text: 'A point outside 3σ',
                        selected: false
                    },
                    {
                        id: 'WE2',
                        text: '2 of 3 data beyond the 2σ limit',
                        selected: false
                    },
                    {
                        id: 'WE3',
                        text: '4 of 5 data beyond the 1σ limit',
                        selected: false
                    },
                    {
                        id: 'WE4',
                        text: '9 data on the same side of the centerline',
                        selected: false
                    }
                ]
            };

            $scope.selectedText = {
                sku: ''
            };

            $scope.CB = {};
            $scope.modalText = "Retrieving required data...";
            angular.element(document).ready(function () {
                var StoredData = qData.GetData();
                if (StoredData == null) {
                    $scope.openModal();
                    $scope.GetCompany().then(function () {
                        $scope.onCompanyChange();
                    });
                }
                else {
                    $scope.Params = StoredData.Params;
                    $scope.DrawChart();
                    $scope.$apply();
                }
            });

            $scope.weRulesModalShowed = false;
            $scope.openWERulesModal = function () {
                if (!$scope.weRulesModalShowed) {
                    weRulesModal.show();
                    $scope.weRulesModalShowed = true;
                }
                else {
                    weRulesModal.hide();
                    $scope.weRulesModalShowed = false;
                    $scope.refreshChart();
                }
            }

            $scope.isModalShowed = false;
            $scope.openModal = function () {
                if (!$scope.isModalShowed) {
                    modal.show();
                    $scope.isModalShowed = true;
                }
            }

            $scope.closeModal = function () {
                if ($scope.isModalShowed) {
                    modal.hide();
                    $scope.isModalShowed = false;
                }
            }

            $scope.GetCompany = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving companies...";
                svc.GetCompany($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Companies = data;
                        if (data.length > 0)
                            $scope.Params.company_id = data[0].company_id;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Companies = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onCompanyChange = function () {
                $scope.openModal();
                $scope.GetMachine().then(function () {
                    $scope.GetSKU();
                });
            }

            $scope.GetMachine = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving machines...";
                $scope.Params.machine_id = '';
                svc.GetMachine($scope.Params.company_id, '', '').
                    success(function (data, status, headers, config) {
                        $scope.CB.Machines = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Machines = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.GetSKU = function () {
                var q = $q.defer();
                $scope.Params.sku_id = '';
                $scope.modalText = "Retrieving SKUs...";
                $scope.CB.Batches = {};
                $scope.CB.Properties = {};
                svc.GetSKU($scope.Params.company_id, '', '','', '').
                    success(function (data, status, headers, config) {
                        $scope.CB.SKUs = data;
                        if (data.length > 0) {
                            $scope.Params.sku_id = data[0].sku_id;
                            $scope.onSKUChange(data[0]);
                        }
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.SKUs = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onSKUChange = function (s) {
                var Text = '';
                if ($scope.CB.SKUs != null && $scope.CB.SKUs.length > 0) {
                    for (var i = 0; i < $scope.CB.SKUs.length; i++) {
                        if ($scope.CB.SKUs[i].sku_id == $scope.Params.sku_id) {
                            Text = $scope.CB.SKUs[i].sku_name;
                            break;
                        }
                    }
                }
                $scope.selectedText.sku = Text;
                $scope.Params.batch_id = [];
                $scope.Params.property_id = [];

                $scope.openModal();
                $scope.GetBatch().then(function () {
                    $scope.GetQualityProperties().then(function () {
                        $scope.closeModal();
                    });
                });
            }

            $scope.GetBatch = function () {
                var q = $q.defer();
                $scope.Params.batch_id = '';
                $scope.modalText = "Retrieving batches...";
                svc.GetBatch($scope.Params.company_id, $scope.Params.sku_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Batches = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Batches = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.GetQualityProperties = function () {
                var q = $q.defer();
                $scope.Params.property_id = '';
                $scope.modalText = "Retrieving quality properties...";
                svc.GetQualityProperties($scope.Params.company_id, $scope.Params.sku_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Properties = data;
                        if (data.length > 0) {
                            $scope.Params.property_id = data[0].property_id;
                        }
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Properties = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.Validate = function () {
                if ($scope.CB.SKUs.length <= 0 || $scope.CB.Properties.length <= 0) {
                    svc.showAlert("Invalid Parameters", "SKU, Quality property cannot be empty");
                    return false;
                }
                return true;
            }

            $scope.chartData = qData.GetData();
            $scope.GetData = function () {
                if ($scope.Validate()) {
                    qData.SetData([]);
                    $scope.modalText = 'Retrieving run chart data...';

                    var param = '';
                    if ($scope.Params.company_id != null && $scope.Params.company_id > 0)
                        param += "&company_id=" + $scope.Params.company_id;
                    if ($scope.Params.machine_id != null && $scope.Params.machine_id.length > 0)
                        param += "&machine_id=" + $scope.Params.machine_id;
                    if ($scope.Params.sku_id != null && $scope.Params.sku_id.length > 0)
                        param += "&sku_id=" + $scope.Params.sku_id;
                    if ($scope.Params.batch_id != null && $scope.Params.batch_id.length > 0)
                        param += "&batch_id=" + $scope.Params.batch_id;
                    if ($scope.Params.property_id != null && $scope.Params.property_id.length > 0)
                        param += "&property_id=" + $scope.Params.property_id;

                    if (param != null && param.length > 0)
                        param = "?" + param.substr(1);

                    $scope.openModal();
                    $http.get(svc.APIServer() + "/QualityRunChart/GetQualityData" + param).
                        success(function (data, status, headers, config) {
                            if (data != null && data.length > 0) {
                                var TempData = [],
                                    PrevData = {
                                        batch_id: '',
                                        machine_id: '',
                                        sku_id: ''
                                    };
                                $scope.chartData = {};
                                $scope.chartData.data = [];
                                $scope.chartData.current_index = 0;
                                for (var i = 0; i < data.length; i++) {
                                    if (data[i].batch_id != PrevData.batch_id || data[i].machine_id != PrevData.machine_id ||
                                        data[i].sku_id != PrevData.sku_id) {
                                        if (TempData.length > 0) {
                                            $scope.chartData.data.push({ data: TempData, violation_checked: false });
                                        }
                                        TempData = [];
                                        PrevData.batch_id = data[i].batch_id;
                                        PrevData.machine_id = data[i].machine_id;
                                        PrevData.sku_id = data[i].sku_id;
                                    }
                                    data[i].violation = false;
                                    TempData.push(data[i]);
                                }

                                if (TempData.length > 0)
                                    $scope.chartData.data.push({ data: TempData, violation_checked: false });
                                $scope.chartData.Params = $scope.Params;
                                qData.SetData($scope.chartData);
                                myNav.pushPage('views/Analysis/quality_run_chart/run_chart_result.html');
                            }
                            else
                                svc.showAlert("Result", "No data found");
                            $scope.closeModal();
                        }).
                        error(function (data, status, headers, config) {
                            $scope.closeModal();
                            svc.showAlert('Error Occured', 'Failed to get run chart data');
                        });
                }
            }

            $scope.refreshChart = function () {
                var Page = myNav.getCurrentPage();
                myNav.pushPage('views/Analysis/quality_run_chart/run_chart_result.html');
                Page.destroy();
            }

            $scope.PrevChart = function () {
                if ($scope.chartData != null && $scope.chartData.data.length > 0 && $scope.chartData.current_index > 0) {
                    $scope.chartData.current_index--;
                    qData.SetData($scope.chartData);
                    $scope.refreshChart();
                }
            }

            $scope.NextChart = function () {
                if ($scope.chartData != null && $scope.chartData.data.length > 0 &&
                    $scope.chartData.current_index < $scope.chartData.data.length - 1) {
                    $scope.chartData.current_index++;
                    qData.SetData($scope.chartData);
                    $scope.refreshChart();
                }
            }

            $scope.chartConfig = {};
            $scope.DrawChart = function () {
                if ($scope.chartData != null && $scope.chartData.data != null &&
                    $scope.chartData.data[$scope.chartData.current_index] != null &&
                    $scope.chartData.data[$scope.chartData.current_index].data != null &&
                    $scope.chartData.data[$scope.chartData.current_index].data.length > 0) {

                    $scope.modalText = 'Generating chart..';
                    $scope.openModal();

                    var SelectedData = $scope.chartData.data[$scope.chartData.current_index];

                    for (var i = 0; i < SelectedData.data.length; i++)
                        SelectedData.data[i].violation = false;

                    for (var i = 0; i < $scope.Params.we_rules.length; i++) {
                        if ($scope.Params.we_rules[i].selected) {
                            switch ($scope.Params.we_rules[i].id) {
                                case 'WE1': $scope.WE1(SelectedData.data); break;
                                case 'WE2': $scope.WE2(SelectedData.data); break;
                                case 'WE3': $scope.WE3(SelectedData.data); break;
                                case 'WE4': $scope.WE4(SelectedData.data); break;
                            }
                        }
                    }
                    $scope.chartData.data[$scope.chartData.current_index] = SelectedData;

                    var Points = [], UCL = [], LCL = [], Target = SelectedData.data[0].target, TargetPoints = [],
                        P2Sigma = [], PSigma = [], M2Sigma = [], MSigma = [],
                        Sigma = SelectedData.data[0].sigma,
                        PropertyName = SelectedData.data[0].property_name, BatchID = SelectedData.data[0].batch_id
                    BatchStart = SelectedData.data[0].batch_start, BatchEnd = SelectedData.data[0].batch_end;

                    for (var i = 0; i < SelectedData.data.length; i++) {
                        Points.push({
                            x: SelectedData.data[i].totalMs,
                            y: SelectedData.data[i].actual_value,
                            marker: {
                                enabled: true,
                                radius: 5
                            }
                        });

                        if (SelectedData.data[i].violation) {
                            Points[i].color = 'red';
                        }

                        UCL.push({ x: SelectedData.data[i].totalMs, y: Target + (3 * Sigma) });
                        P2Sigma.push({ x: SelectedData.data[i].totalMs, y: Target + (2 * Sigma) });
                        PSigma.push({ x: SelectedData.data[i].totalMs, y: Target + (1 * Sigma) });
                        TargetPoints.push({ x: SelectedData.data[i].totalMs, y: Target });
                        MSigma.push({ x: SelectedData.data[i].totalMs, y: Target - (1 * Sigma) });
                        M2Sigma.push({ x: SelectedData.data[i].totalMs, y: Target - (2 * Sigma) });
                        LCL.push({ x: SelectedData.data[i].totalMs, y: Target - (3 * Sigma) });
                    }

                    var DataSeries = [
                        {
                            name: "Value",
                            data:Points
                        },
                        {
                            name: "UCL",
                            data: UCL
                        },
                        {
                            name: "+2σ",
                            data: P2Sigma
                        },
                        {
                            name: "+1σ",
                            data: PSigma
                        },
                        {
                            name: "Target",
                            data: TargetPoints
                        },
                        {
                            name: "-1σ",
                            data: MSigma
                        },
                        {
                            name: "-2σ",
                            data: M2Sigma
                        },
                        {
                            name: "LCL",
                            data: LCL
                        }
                    ];

                    var Config = {
                        options: {
                            chart: {
                                zoomType: 'x',
                                type: 'line'
                            },
                            rangeSelector: {
                                buttons: [
                                    {
                                        type: 'day',
                                        count: 1,
                                        text: '1 Day'
                                    },
                                    {
                                        type: 'week',
                                        count: 1,
                                        text: '1 Week'
                                    },
                                    {
                                        type: 'all',
                                        text: 'All'
                                    }
                                ],
                                selected: 0,
                                buttonSpacing: 10,
                                inputEnabled: false,
                                buttonTheme: {
                                    width: null
                                }
                            },
                            navigator: {
                                enabled: false
                            },
                            legend: {
                                enabled: true
                            }
                        },
                        title: {
                            text: "Run Chart For " + PropertyName
                        },
                        subtitle: {
                            text: "Batch: " + BatchID + "<br/>From " + BatchStart + " until " + BatchEnd
                        },
                        xAxis: {
                            type: 'datetime',
                            title: {
                                text: 'Timestamp'
                            },
                            dateTimeLabelFormats: {
                                day: '%e %b %Y %H:%m'
                            }
                        },
                        yAxis: {
                            title: {
                                text: PropertyName
                            }
                        },
                        series: DataSeries,
                        useHighStocks: true
                    };

                    $scope.chartConfig = Config;
                    $scope.closeModal();
                }
            }

            $scope.WE1 = function (data)
            {
                if (data != null && data.length > 0) {
                    var Target = Number(data[0].target), Sigma = Number(data[0].sigma);
                    for (var i = 0; i < data.length; i++) {
                        if (Number(data[i].actual_value) > (Target + (3 * Sigma)) ||
                            Number(data[i].actual_value) < (Target - (3 * Sigma)))
                                data[i].violation = true;
                    }
                }
            }

            $scope.WE2 = function (data) {
                if (data != null && data.length > 2) {
                    var Target = Number(data[0].target), Sigma = Number(data[0].sigma);

                    for (var i = 2; i < data.length; i++) {
                        var UpperCount = 0, LowerCount = 0;
                        for (var j = i - 2; j <= i; j++) {
                            if (Number(data[j].actual_value) > (Target + (2 * Sigma))) {
                                UpperCount++;
                                if (UpperCount == 2)
                                    break;
                            }
                            else if (Number(data[j].actual_value) < (Target - (2 * Sigma))) {
                                LowerCount++;
                                if (LowerCount == 2)
                                    break;
                            }
                        }

                        if (UpperCount == 2 || LowerCount == 2)
                            data[i].violation = true;
                    }
                }
            }

            $scope.WE3 = function (data) {
                if (data != null && data.length > 4) {
                    var Target = Number(data[0].target), Sigma = Number(data[0].sigma);

                    for (var i = 4; i < data.length; i++) {
                        var UpperCount = 0, LowerCount = 0;
                        for (var j = i - 4; j <= i; j++) {
                            if (Number(data[j].actual_value) > (Target + Sigma)) {
                                UpperCount++;
                                if (UpperCount == 4)
                                    break;
                            }
                            else if (Number(data[j].actual_value) < (Target - Sigma)) {
                                LowerCount++;
                                if (LowerCount == 4)
                                    break;
                            }
                        }

                        if (UpperCount == 4 || LowerCount == 4)
                            data[i].violation = true;
                    }
                }
            }

            $scope.WE4 = function (data) {
                if (data != null && data.length > 8) {
                    var Target = Number(data[0].target);

                    for (var i = 8; i < data.length; i++) {
                        var UpperCount = 0, LowerCount = 0;
                        for (var j = i - 8; j <= i; j++) {
                            if (Number(data[j].actual_value) > Target) {
                                UpperCount++;
                                if (UpperCount == 9)
                                    break;
                            }
                            else if (Number(data[j].actual_value) < Target) {
                                LowerCount++;
                                if (LowerCount == 9)
                                    break;
                            }
                        }

                        if (UpperCount == 9 || LowerCount == 9)
                            data[i].violation = true;
                    }
                }
            }
        }]);
})();