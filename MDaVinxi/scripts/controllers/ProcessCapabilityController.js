﻿(function () {
    var app = angular.module('mDavinxi');

    app.controller("ProcessCapabilityController",
        ['$scope', 'svc', '$q', '$http', 'qData', '$filter',
        function ($scope, svc, $q, $http, qData, $filter) {
            var LoginInfo = JSON.parse(svc.getAuth());
            $scope.Params = {
                company_id: LoginInfo.company_id,
                division_id: '',
                block_id: '',
                machine_id: '',
                category_id: '',
                subcategory_id: '',
                business_id: '',
                segment_id: '',
                sku_id: '',
                batch_id: '',
                property_id: ''
            };

            $scope.selectedText = {
                machine: '',
                sku: '',
                property: ''
            };

            $scope.CB = {};
            $scope.modalText = "Retrieving required data...";
            angular.element(document).ready(function () {
                $scope.openModal();
                $scope.GetCompany().then(function () {
                    $scope.onCompanyChange();
                });
            });

            $scope.isModalShowed = false;
            $scope.openModal = function () {
                if (!$scope.isModalShowed) {
                    modal.show();
                    $scope.isModalShowed = true;
                }
            }

            $scope.closeModal = function () {
                if ($scope.isModalShowed) {
                    modal.hide();
                    $scope.isModalShowed = false;
                }
            }

            $scope.GetCompany = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving companies...";
                svc.GetCompany($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Companies = data;
                        if (data.length > 0)
                            $scope.Params.company_id = data[0].company_id;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Companies = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onCompanyChange = function () {
                $scope.openModal();
                $scope.GetDivision().then(function () {
                    $scope.GetBlock().then
                    (
                        function () {
                            $scope.GetBlock().then
                                (
                                    function () {
                                        $scope.onBlockChange();
                                    }
                                );
                        }
                    ).then(function () {
                        $scope.GetSKUSegment().then(function () {
                            $scope.GetSKUBusiness().then(function () {
                                $scope.GetSKUCategory().then(function () {
                                    $scope.onSKUCategoryChange();
                                });
                            });
                        });
                    });
                });
            }

            $scope.GetDivision = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving divisions...";
                $scope.Params.division_id = '';
                svc.GetDivision($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Divisions = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Divisions = {};
                        q.reject();
                    });
                return q.promise;
            }
          
            $scope.onDivisionChange = function ()
            {
                $scope.openModal();
                $scope.GetBlock().then
                    (
                        function ()
                        {
                            $scope.GetBlock().then
                                (
                                    function () {
                                        $scope.onBlockChange();
                                    }
                                );
                        }
                    );
            }

            $scope.GetBlock = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving blocks...";
                $scope.Params.block_id = '';
                svc.GetBlock($scope.Params.company_id, $scope.Params.division_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Blocks = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Blocks = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onBlockChange = function () {
                $scope.openModal();
                var promise = $scope.GetMachine();
                promise.then(function () { $scope.closeModal(); });
                return promise;
            }

            $scope.GetMachine = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving machines...";
                $scope.Params.machine_id = '';
                svc.GetMachine($scope.Params.company_id, $scope.Params.division_id, $scope.Params.block_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Machines = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Machines = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.GetSKUCategory = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving SKU categories...";
                $scope.Params.category_id = '';
                svc.GetSKUCategory($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Categories = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Categories = {};
                        q.reject();
                    });
                return q.promise;
            }
            
            $scope.onSKUCategoryChange = function () {
                $scope.Params.subcategory_id = '';

                $scope.openModal();
                $scope.GetSKUSubCategory().then
                    (
                        function () {
                            $scope.onSKUSubCategoryChange();
                        }
                    );
            }

            $scope.GetSKUSubCategory = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving SKU subcategories...";
                $scope.Params.subcategory_id = '';
                svc.GetSKUSubCategory($scope.Params.company_id, $scope.Params.category_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.SubCategories = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.SubCategories = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onSKUSubCategoryChange = function () {
                $scope.Params.sku_id = '';
                $scope.openModal();
                $scope.GetSKU().then
                    (
                        function () {
                            $scope.closeModal();
                        }
                    );
            }

            $scope.GetSKUSegment = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving SKU segments...";
                $scope.Params.segment_id = '';
                svc.GetSKUSegment($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Segments = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Segments = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.GetSKUBusiness = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving SKU businesses...";
                $scope.Params.business_id = '';
                svc.GetSKUBusiness($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Business = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Business = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.GetSKU = function () {
                var q = $q.defer();
                $scope.Params.sku_id = '';
                $scope.modalText = "Retrieving SKUs...";
                $scope.CB.Batches = {};
                $scope.CB.Properties = {};
                svc.GetSKU($scope.Params.company_id, $scope.Params.category_id, $scope.Params.subcategory_id,
                    $scope.Params.segment_id, $scope.Params.business_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.SKUs = data;
                        if (data.length > 0) {
                            $scope.Params.sku_id = data[0].sku_id;
                            $scope.onSKUChange();
                        }
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.SKUs = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onSKUChange = function () {
                var Text = '';
                if ($scope.CB.SKUs != null && $scope.CB.SKUs.length > 0) {
                    for (var i = 0; i < $scope.CB.SKUs.length; i++) {
                        if ($scope.CB.SKUs[i].sku_id == $scope.Params.sku_id) {
                            Text = $scope.CB.SKUs[i].sku_name;
                            break;
                        }
                    }
                }
                $scope.selectedText.sku = Text;
                $scope.Params.batch_id = '';
                $scope.Params.property_id = '';

                $scope.openModal();
                $scope.GetBatch().then(function () {
                    $scope.QualityProperties().then(function () {
                        $scope.closeModal();
                    });
                });
            }

            $scope.GetBatch = function () {
                var q = $q.defer();
                $scope.Params.batch_id = '';
                $scope.modalText = "Retrieving batches...";
                svc.GetBatch($scope.Params.company_id, $scope.Params.sku_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Batches = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Batches = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.QualityProperties = function () {
                var q = $q.defer();
                $scope.Params.property_id = '';
                $scope.modalText = "Retrieving quality properties...";
                svc.GetQualityProperties($scope.Params.company_id, $scope.Params.sku_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Properties = data;
                        if (data.length > 0) {
                            $scope.Params.property_id = data[0].property_id;
                            $scope.onPropertyChanged(data[0]);
                        }
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Properties = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onPropertyChanged = function ()
            {
                var Text = '';
                if ($scope.CB.Properties != null && $scope.CB.Properties.length > 0) {
                    for (var i = 0; i < $scope.CB.Properties.length; i++) {
                        if ($scope.CB.Properties[i].property_id == $scope.Params.property_id) {
                            Text = $scope.CB.Properties[i].property_name;
                            break;
                        }
                    }
                }
                $scope.selectedText.property = Text;
            }

            $scope.Validate = function () {
                if ($scope.CB.SKUs.length <= 0 || $scope.CB.Batches.length <= 0 || $scope.CB.Properties.length <= 0)
                {
                    svc.showAlert("Invalid Parameters", "SKU, Batch and Quality property cannot be empty");
                    return false;
                }
                return true;
            }

            $scope.chartConfig = qData.GetData();
            $scope.GetChart = function () {
                if ($scope.Validate()) {
                    qData.SetData({});
                    $scope.modalText = 'Retrieving process capability data...';
                    $scope.openModal();
                    $http.get(svc.APIServer() + "/ProcessCapability/GetCapability",
                                { params: $scope.Params }).
                        success(function (data, status, headers, config) {
                            $scope.modalText = 'Generating chart...';
                            var PlotLines = [
                                    {
                                        value: data.oData[0].propertySetting.target,
                                        width: 2,
                                        dashStyle: "Dash",
                                        color: "green",
                                        zIndex: 5
                                    },
                                    {
                                        value: data.oData[0].propertySetting.lrl,
                                        width: 2,
                                        dashStyle: "Dash",
                                        color: "red",
                                        zIndex: 5
                                    },
                                    {
                                        value: data.oData[0].propertySetting.url,
                                        width: 2,
                                        dashStyle: "Dash",
                                        color: "red",
                                        zIndex: 5
                                    }
                            ];

                            var Hist = new Array();
                            Hist.push([data.MinOutside.point, data.MinOutside.count]);
                            Hist.push([data.Min3s.point, data.Min3s.count]);
                            Hist.push([data.Min2s.point, data.Min2s.count]);
                            Hist.push([data.Min1s.point, data.Min1s.count]);
                            Hist.push([data.Max1s.point, data.Max1s.count]);
                            Hist.push([data.Max2s.point, data.Max2s.count]);
                            Hist.push([data.Max3s.point, data.Max3s.count]);
                            Hist.push([data.MaxOutside.point, data.MaxOutside.count]);

                            var Dist = new Array(), Target = Number(data.target), StdDev = Number(data.stdDev),
                                UpperLimit = Target + (3 * StdDev), LowerLimit = Target - (3 * StdDev);
                            for (var i = LowerLimit ; i <= UpperLimit ; i += StdDev / 100) {
                                var Value = Math.exp(-1 * (Math.pow(i - Target, 2) / (2 * StdDev * StdDev))) / (StdDev * (Math.sqrt(2 * Math.PI)));
                                Dist.push([i, Value]);
                            }

                            qData.SetData({
                                options: {
                                    chart: {
                                        zoomType: 'xy',
                                        height: 450
                                    },
                                    tooltip: {
                                        borderWidth: 1,
                                        formatter: function () {
                                            if (this.series.name == 'Frequency')
                                                return "<b>" + this.series.name + " : </b>" + this.y;
                                            return false;
                                        },
                                        valueDecimals: 3
                                    }
                                },
                                title: {
                                    text: $scope.selectedText.sku + "<br/>" + "Process Capability"
                                },
                                subtitle: {
                                    text: "Quality Property: " + $scope.selectedText.property + "<br/>" + "Cp = " + data.cp + " ; " +
                                        "Cpk = " + data.cpk + "<br/>" +
                                        "Pp = " + data.pp + " ; " + "Ppk = " + data.ppk
                                },
                                plotOptions: {
                                    series: {
                                        marker: {
                                            minPointLength: 1,
                                            shadow: false,
                                            enabled: false
                                        }
                                    }
                                },
                                xAxis: {
                                    plotLines: PlotLines
                                },
                                yAxis: [
                                    {
                                        title: {
                                            text: '<b>Frequency</b>',
                                            style: {
                                                color: Highcharts.getOptions().colors[0],
                                                fontSize: '100%'
                                            }
                                        }
                                    },
                                    {
                                        title: {
                                            text: '<b>Distribution</b>',
                                            style: {
                                                color: Highcharts.getOptions().colors[1],
                                                fontSize: '100%'
                                            }
                                        },
                                        opposite: true,
                                        min: 0
                                    }
                                ],
                                series: [
                                    {
                                        name: 'Frequency',
                                        type: 'column',
                                        yAxis: 0,
                                        data: Hist,
                                        pointRange: StdDev,
                                        groupPadding: 0,
                                        pointPadding: 0,
                                        borderWidth: .5
                                    },
                                    {
                                        name: 'Distribution',
                                        type: 'spline',
                                        yAxis: 1,
                                        data: Dist
                                    }
                                ]
                            });
                            $scope.closeModal();
                            myNav.pushPage('views/Analysis/process_capability/process_capability_result.html');
                        }).
                        error(function (data, status, headers, config) {
                            $scope.closeModal();
                            svc.showAlert('Error Occured', 'Failed to draw process capability chart');
                        });
                }
            }
        }]);
})();