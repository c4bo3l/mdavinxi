﻿(function () {
    var app = angular.module('mDavinxi');

    app.controller("AnalysisController", ['$scope', 'qData', function ($scope, qData) {
        $scope.Menus =
            [
                {
                    title: 'Quality',
                    submenus: [
                        {
                            title: 'Run Chart',
                            url: 'views/Analysis/quality_run_chart/run_chart.html'
                        },
                        {
                            title: 'Defect Tracking',
                            url: 'views/Analysis/defect_tracking/defect_tracking.html'
                        },
                        {
                            title: 'Process Capability',
                            url: 'views/Analysis/process_capability/process_capability.html'
                        }
                    ]
                },
                {
                    title: 'Production',
                    submenus: [
                        {
                            title: 'Downtime Tracking',
                            url: 'views/Analysis/downtime_tracking/downtime_tracking.html'
                        }
                    ]
                }
            ];

        $scope.showPage = function (parentIndex, index) {
            qData.SetData(null);
            myNav.pushPage($scope.Menus[parentIndex].submenus[index].url);
        }

        //$scope.ProcessCapability = {};
    }]);
})();