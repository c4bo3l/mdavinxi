﻿(function () {
    var app = angular.module('mDavinxi');

    app.controller("SignInController", ['$scope','$http', 'svc', function ($scope,$http, svc) {
        $scope.signInData = {
            UserName: null,
            Password: null,
            RememberMe: false
        };

        $scope.signIn = function () {
            modal.show();
            $http.get(svc.APIServer() + '/User/LoginJSON', { params: $scope.signInData }).
              success(function (data, status, headers, config) {
                  modal.hide();
                  if (data != null && data.status == 'OK') {
                      svc.setAuth(JSON.stringify(data.data));
                      menu.setMainPage("views/Production/index.html");
                  }
                  else {
                      svc.showAlert("Sign-in failed", "The user name or password provided is incorrect");
                  }
              }).
              error(function (data, status, headers, config) {
                  modal.hide();
                  svc.showAlert("Error Occured", "Failed to retrieve log-in information");
              });
        }
    }]);
})();