﻿(function () {
    var app = angular.module('mDavinxi');

    app.controller("SettingsController", ['svc', function (svc) {
        this.APIServer = svc.APIServer();

        this.isAuth = function ()
        {
            return svc.isAuth();
        }

        this.setAPIServer = function () {
            if (this.APIServer.length > 0)
                localStorage.setItem("davinxi-api-server", this.APIServer);
            else
                localStorage.removeItem("davinxi-api-server");
        }
    }]);
})();