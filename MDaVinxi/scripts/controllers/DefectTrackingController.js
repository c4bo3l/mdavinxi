﻿(function () {
    var app = angular.module('mDavinxi');

    app.controller("DefectTrackingController",
        ['$scope', 'svc', '$q', '$http', 'qData', '$filter', '$cordovaDatePicker',
        function ($scope, svc, $q, $http, qData, $filter, $cordovaDatePicker) {
            var LoginInfo = JSON.parse(svc.getAuth()),
                StartDate = new Date(), FinishDate = new Date();
            $scope.Params = {
                company_id: LoginInfo.company_id,
                division_id: '',
                chkDivision: false,
                block_id: '',
                chkBlock: false,
                machine_id: '',
                chkMachine: false,
                category_id: '',
                chkCategory: false,
                subcategory_id: '',
                chkSubcategory: false,
                business_id: '',
                chkBusiness: false,
                segment_id: '',
                chkSegment: false,
                sku_id: '',
                chkSKU: false,
                batch_id: '',
                chkBatch: false,
                severity: 4,
                targetParentDefect: '',
                startDate: '',
                finishDate: '',
                timeNumber: 1,
                timeGroupedType: 1
            };

            $scope.Severity = [
                {
                    id: 0,
                    text:'No defect'
                },
                {
                    id: 1,
                    text: 'Just noticeable'
                },
                {
                    id: 2,
                    text: 'Warning'
                },
                {
                    id: 3,
                    text: 'Downgrade'
                },
                {
                    id: 4,
                    text: 'Reject'
                }
            ];

            $scope.TimeType = [
                {
                    id: 0,
                    text: 'Hour(s)'
                },
                {
                    id: 1,
                    text: 'Day(s)'
                },
                {
                    id: 2,
                    text: 'Week(s)'
                },
                {
                    id: 3,
                    text: 'Month(s)'
                },
                {
                    id: 4,
                    text: 'Year(s)'
                }
            ];

            $scope.showDateTimePicker = function (dateType) {
                var DateTime = dateType == 0 ? StartDate : FinishDate;
                var options = { date: DateTime, mode: 'date' };
                $cordovaDatePicker.show(options).then(function (date) {
                    if (date != null) {
                        DateTime.setFullYear(date.getFullYear());
                        DateTime.setMonth(date.getMonth());
                        DateTime.setDate(date.getDate());

                        options = { date: DateTime, mode: 'time' };
                        $cordovaDatePicker.show(options).then(function (date) {
                            if (date != null) {
                                DateTime.setHours(date.getHours());
                                DateTime.setMinutes(date.getMinutes());
                                if (dateType == 0)
                                    StartDate = DateTime;
                                else
                                    FinishDate = DateTime;
                                $scope.refreshDate();
                            }
                        });
                    }
                });
            }

            $scope.refreshDate = function () {
                $scope.Params.startDate = $filter('date')(StartDate, 'MMM dd yyyy HH:mm');
                $scope.Params.finishDate = $filter('date')(FinishDate, 'MMM dd yyyy HH:mm');
            }

            $scope.selectedText = {
                property: 'All'
            };

            $scope.CB = {};
            $scope.modalText = "Retrieving required data...";
            angular.element(document).ready(function () {
                if (qData.GetData() == null) {
                    $scope.openModal();
                    $scope.GetCompany().then(function () {
                        $scope.onCompanyChange();
                    });
                    StartDate.setDate(StartDate.getDate() - 1);
                    $scope.refreshDate();
                }
            });

            $scope.isModalShowed = false;
            $scope.openModal = function () {
                if (!$scope.isModalShowed) {
                    if (modal != null)
                        modal.show();
                    else
                        result_modal.show();
                    $scope.isModalShowed = true;
                }
            }

            $scope.closeModal = function () {
                if ($scope.isModalShowed) {
                    if (modal != null)
                        modal.hide();
                    else
                        result_modal.hide();
                    $scope.isModalShowed = false;
                }
            }

            $scope.GetCompany = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving companies...";
                svc.GetCompany($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Companies = data;
                        if (data.length > 0)
                            $scope.Params.company_id = data[0].company_id;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Companies = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onCompanyChange = function () {
                $scope.openModal();
                $scope.GetDivision().then(function () {
                    $scope.GetBlock().then
                    (
                        function () {
                            $scope.GetBlock().then
                                (
                                    function () {
                                        $scope.onBlockChange();
                                    }
                                );
                        }
                    ).then(function () {
                        $scope.GetSKUSegment().then(function () {
                            $scope.GetSKUBusiness().then(function () {
                                $scope.GetSKUCategory().then(function () {
                                    $scope.onSKUCategoryChange();
                                });
                            });
                        });
                    });
                });
            }

            $scope.GetDivision = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving divisions...";
                $scope.Params.division_id = '';
                svc.GetDivision($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Divisions = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Divisions = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onDivisionChange = function () {
                $scope.openModal();
                $scope.GetBlock().then
                    (
                        function () {
                            $scope.GetBlock().then
                                (
                                    function () {
                                        $scope.onBlockChange();
                                    }
                                );
                        }
                    );
            }

            $scope.GetBlock = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving blocks...";
                $scope.Params.block_id = '';
                svc.GetBlock($scope.Params.company_id, $scope.Params.division_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Blocks = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Blocks = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onBlockChange = function () {
                $scope.openModal();
                var promise = $scope.GetMachine();
                promise.then(function () { $scope.closeModal(); });
                return promise;
            }

            $scope.GetMachine = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving machines...";
                $scope.Params.machine_id = '';
                svc.GetMachine($scope.Params.company_id, $scope.Params.division_id, $scope.Params.block_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Machines = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Machines = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.GetSKUCategory = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving SKU categories...";
                $scope.Params.category_id = '';
                svc.GetSKUCategory($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Categories = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Categories = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onSKUCategoryChange = function () {
                $scope.Params.subcategory_id = '';

                $scope.openModal();
                $scope.GetSKUSubCategory().then
                    (
                        function () {
                            $scope.onSKUSubCategoryChange();
                        }
                    );
            }

            $scope.GetSKUSubCategory = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving SKU subcategories...";
                $scope.Params.subcategory_id = '';
                svc.GetSKUSubCategory($scope.Params.company_id, $scope.Params.category_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.SubCategories = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.SubCategories = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onSKUSubCategoryChange = function () {
                $scope.Params.sku_id = '';
                $scope.openModal();
                $scope.GetSKU().then
                    (
                        function () {
                            $scope.closeModal();
                        }
                    );
            }

            $scope.GetSKUSegment = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving SKU segments...";
                $scope.Params.segment_id = '';
                svc.GetSKUSegment($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Segments = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Segments = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.GetSKUBusiness = function () {
                var q = $q.defer();
                $scope.modalText = "Retrieving SKU businesses...";
                $scope.Params.business_id = '';
                svc.GetSKUBusiness($scope.Params.company_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Business = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Business = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.GetSKU = function () {
                var q = $q.defer();
                $scope.Params.sku_id = '';
                $scope.modalText = "Retrieving SKUs...";
                $scope.CB.Batches = {};
                $scope.CB.Properties = {};
                svc.GetSKU($scope.Params.company_id, $scope.Params.category_id, $scope.Params.subcategory_id,
                    $scope.Params.segment_id, $scope.Params.business_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.SKUs = data;
                        if (data.length > 0) {
                            $scope.onSKUChange(data[0]);
                        }
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.SKUs = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onSKUChange = function () {
                var Text = '';
                if ($scope.CB.SKUs != null && $scope.CB.SKUs.length > 0) {
                    for (var i = 0; i < $scope.CB.SKUs.length; i++) {
                        if ($scope.CB.SKUs[i].sku_id == $scope.Params.sku_id) {
                            Text = $scope.CB.SKUs[i].sku_name;
                            break;
                        }
                    }
                }
                $scope.selectedText.sku = Text;
                $scope.Params.batch_id = '';
                $scope.Params.property_id = '';

                $scope.openModal();
                $scope.GetBatch().then(function () {
                    $scope.closeModal();
                });
            }

            $scope.GetBatch = function () {
                var q = $q.defer();
                $scope.Params.batch_id = '';
                $scope.modalText = "Retrieving batches...";
                svc.GetBatch($scope.Params.company_id, $scope.Params.sku_id).
                    success(function (data, status, headers, config) {
                        $scope.CB.Batches = data;
                        q.resolve();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.CB.Batches = {};
                        q.reject();
                    });
                return q.promise;
            }

            $scope.onBatchChange = function ()
            {
                if ($scope.CB.Batches != null && $scope.CB.Batches.length > 0)
                {
                    for (var i = 0; i < $scope.CB.Batches.length; i++) {
                        if ($scope.CB.Batches[i].batch_id == $scope.Params.batch_id)
                        {
                            StartDate = new Date($scope.CB.Batches[i].start_date);
                            FinishDate = new Date($scope.CB.Batches[i].finish_date);
                            $scope.refreshDate();
                            break;
                        }
                    }
                }
            }

            $scope.chartConfig = qData.GetData();
            if ($scope.chartConfig != null && $scope.chartConfig.Params != null)
                $scope.Params = $scope.chartConfig.Params;
                

            $scope.Process = function () {
                $scope.modalText = 'Generating chart...';
                $scope.chartConfig = {};
                $scope.chartConfig.Breadcrumbs = [];
                qData.SetData($scope.chartConfig);
                $scope.Params.targetParentDefect = '';
                $scope.GetChart().then(function () {
                    myNav.pushPage('views/Analysis/defect_tracking/defect_tracking_result.html');
                }, function () {
                    svc.showAlert("Result Confirmation", "No data");
                });
            }

            $scope.GetChart = function () {
                modal.show();
                var q = $q.defer();

                $http.get(svc.APIServer() + "/DefectTracking/GetDefectTracking",
                            { params: $scope.Params }).
                    success(function (data, status, headers, config) {
                        if (data != null && data.pData != null && data.pData.length > 0) {
                            $scope.InitPChart(data.pData);
                            if (data != null && data.rawData != null && data.rawData.length > 0) {
                                $scope.InitFreqPareto(data.rawData);
                                $scope.InitWeightPareto(data.rawData);
                            }
                            $scope.chartConfig.Params = $scope.Params;
                            qData.SetData($scope.chartConfig);
                            q.resolve();
                            modal.hide();
                        }
                        else {
                            q.reject();
                            modal.hide();
                        }
                    }).
                    error(function (data, status, headers, config) {
                        q.reject();
                        modal.hide();
                        svc.showAlert('Error Occured', 'Failed to draw defect tracking chart');
                    });
                return q.promise;
            }

            $scope.InitPChart = function (data) {
                if (data != null && data.length > 0) {
                    if ($scope.chartConfig != null && $scope.chartConfig.pChart != null)
                        $scope.chartConfig.pChart.loading = true;

                    var SumProduct = 0, SumDefectedProduct = 0, Points = new Array(), CLPoints = new Array(),
                        UCLPoints = new Array(), LCLPoints = new Array(), XCategories = new Array();

                    for (var i = 0; i < data.length; i++) {
                        SumProduct += Number(data[i].total_product);
                        SumDefectedProduct += Number(data[i].count_defect_product);
                        Points.push(Number(data[i].fraction_defective));
                        XCategories.push(data[i].formated_start_date + "<br/>" + data[i].xName.replace('-', '<br/>'));
                    }
                    var CL = SumDefectedProduct / SumProduct;
                    var PlotLines = [
                        {
                            value: CL,
                            width: 2,
                            dashStyle: "Dash",
                            color: "green",
                            zIndex: 5
                        },
                        {
                            value: 1,
                            width: 2,
                            dashStyle: "Dash",
                            color: "red",
                            zIndex: 5
                        },
                        {
                            value: 0,
                            width: 2,
                            dashStyle: "Dash",
                            color: "red",
                            zIndex: 5
                        }
                    ];

                    var config = {
                        options: {
                            chart: {
                                type: 'line',
                                zoomType: 'xy',
                                height: 450
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size: 10px">{point.key.formated_start_date}</span><br/>'
                            }
                        },
                        title: {
                            text: "P-Chart"
                        },
                        subtitle: {
                            text: "Defect Category: " + ($scope.chartConfig.Breadcrumbs.length <= 0 ? "All" :
                                $scope.chartConfig.Breadcrumbs[$scope.chartConfig.Breadcrumbs.length - 1].defect_name)
                        },
                        xAxis: {
                            categories: data,
                            labels: {
                                formatter: function () {
                                    return this.value.formated_start_date + "<br/>" + this.value.xName.replace('-', '<br/>');
                                },
                                rotation: 90
                            }
                        },
                        yAxis: {
                            title: {
                                text: 'Defect Fraction'
                            },
                            plotLines: PlotLines
                        },
                        series: [
                            {
                                name: "p",
                                data: Points,
                            }
                        ]
                    };
                    $scope.chartConfig.pChart = config;
                }
            }

            $scope.InitPareto = function (data, points, percentage, isWeight) {
                return {
                    options: {
                        chart: {
                            type: 'column',
                            zoomType: 'xy',
                            height: 450
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size: 10px">{point.key.defect_name}</span><br/>',
                            shared: true
                        },
                        plotOptions: {
                            column: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function () {
                                            var selectedData = (isWeight ? $scope.WeightData : $scope.FreqData);
                                            if ($scope.chartConfig.Breadcrumbs.length <= 0 ||
                                                ($scope.chartConfig.Breadcrumbs.length > 0 &&
                                                $scope.chartConfig.Breadcrumbs[$scope.chartConfig.Breadcrumbs.length - 1].defect_id !=
                                                selectedData[this.x].defect_id)) {
                                                $scope.modalText = "Retrieving " + selectedData[this.x].defect_name + " data...";
                                                result_modal.show();
                                                $scope.chartConfig.Breadcrumbs.push(selectedData[this.x]);
                                                $scope.Params.targetParentDefect = selectedData[this.x].defect_id;
                                                $scope.GetChart().then(function () {
                                                    tabbar.setActiveTab(tabbar.getActiveTabIndex());
                                                    result_modal.hide();
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    title: {
                        text: "Defect " + (isWeight ? "Weight" : "Frequency") + " Tracking"
                    },
                    subtitle: {
                        text: "Defect Category : " + ($scope.chartConfig.Breadcrumbs.length <= 0 ? "All" :
                                $scope.chartConfig.Breadcrumbs[$scope.chartConfig.Breadcrumbs.length - 1].defect_name)
                    },
                    xAxis: {
                        categories: data,
                        labels: {
                            formatter: function () {
                                return this.value.defect_name;
                            },
                            rotation: 90
                        }
                    },
                    yAxis: [
                        {
                            gridLineWidth: 0,
                            title: {
                                text: "Percentage",
                                style: {
                                    color: '#4572A7'
                                }
                            },
                            labels: {
                                formatter: function () {
                                    return this.value + ' %';
                                },
                                style: {
                                    color: '#4572A7'
                                }
                            },
                            opposite: true,
                            tickInterval: 10,
                            min: 0,
                            max: 100
                        },
					    {
					        title: {
					            text: !isWeight ? "Frequency" : "Weight",
					            style: {
					                color: '#89A54E'
					            }
					        }
					    }
                    ],
                    series: [
                        {
                            name: !isWeight ? "Frequency" : "Weight",
                            type: 'column',
                            yAxis: 1,
                            data: points
                        },
                        {
                            name: "Percentage",
                            type: 'spline',
                            data: percentage,
                            tooltip: {
                                valueSuffix: ' %'
                            }
                        }
                    ]
                };
            }

            $scope.FreqData = {};
            $scope.InitFreqPareto = function (data) {
                if (data != null && data.length > 0) {
                    if ($scope.chartConfig != null && $scope.chartConfig.freqChart != null)
                        $scope.chartConfig.freqChart.loading = true;
                    data.sort
                        (
                            function (a, b) {
                                return Number(b.sum_count) - Number(a.sum_count);
                            }
                        );
                    $scope.FreqData = data;
                    var Sum = 0;
                    for (var i = 0; i < data.length; i++) {
                        Sum += Number(data[i].sum_count);
                    }

                    var Percentage = [], Points = [], CurrentSum = 0;
                    for (var i = 0; i < data.length; i++) {
                        Points.push(Number(data[i].sum_count));
                        CurrentSum += Number(data[i].sum_count);
                        Percentage.push((CurrentSum / Sum) * 100.0);
                    }

                    $scope.chartConfig.freqChart = $scope.InitPareto(data, Points, Percentage, false);
                }
            }

            $scope.WeightData = {};
            $scope.InitWeightPareto = function (data) {
                if (data != null && data.length > 0) {
                    if ($scope.chartConfig != null && $scope.chartConfig.weightChart != null)
                        $scope.chartConfig.weightChart.loading = true;
                    data.sort
                        (
                            function (a, b) {
                                return Number(b.sum_weight) - Number(a.sum_weight);
                            }
                        );
                    $scope.WeightData = data;
                    var Sum = 0;
                    for (var i = 0; i < data.length; i++) {
                        Sum += Number(data[i].sum_weight);
                    }

                    var Percentage = [], Points = [], CurrentSum = 0;
                    for (var i = 0; i < data.length; i++) {
                        Points.push(Number(data[i].sum_weight));
                        CurrentSum += Number(data[i].sum_weight);
                        Percentage.push((CurrentSum / Sum) * 100.0);
                    }
                    
                    $scope.chartConfig.weightChart = $scope.InitPareto(data, Points, Percentage, true);
                }
            }

            $scope.back = function () {
                if ($scope.chartConfig.Breadcrumbs.length <= 0)
                    myNav.popPage();
                else
                {
                    $scope.chartConfig.Breadcrumbs.pop();
                    var DefectID = '';
                    if ($scope.chartConfig.Breadcrumbs.length > 0)
                        DefectID = $scope.chartConfig.Breadcrumbs[$scope.chartConfig.Breadcrumbs.length - 1].defect_id;
                    $scope.chartConfig.Params.targetParentDefect = DefectID;
                    $scope.Params = $scope.chartConfig.Params;
                    $scope.modalText = "Retrieving previous data...";
                    result_modal.show();
                    $scope.GetChart().then(function () { tabbar.setActiveTab(tabbar.getActiveTabIndex()); result_modal.hide(); });
                }
            }
        }]);
})();