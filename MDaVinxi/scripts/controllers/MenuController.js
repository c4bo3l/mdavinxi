﻿(function () {
    var app = angular.module('mDavinxi');

    app.controller("MenuController", ['svc', 'qData', function (svc, qData) {

        this.Menus = [
            {
                text: 'Production',
                icon: 'ion-cube',
                url: 'views/Production/index.html'
            },
            {
                text: 'Analysis',
                icon: 'ion-stats-bars',
                url: 'views/Analysis/index.html'
            },
            {
                text: 'Settings',
                icon: 'ion-ios-cog',
                url: 'views/settings.html'
            },
            {
                text: 'Sign out',
                icon: 'fa-sign-out',
                url: 'signOut'
            }
        ];

        this.signOut = function () {
            svc.removeAuth();
            menu.setMainPage("views/signin/login.html", { closeMenu: true });
        }

        this.isAuth = function () {
            return svc.isAuth();
        }

        this.getName = function () {
            return svc.getAuth() != null ? svc.getAuth().name : "";
        }

        this.goToPage = function (index) {
            qData.SetData(null);
            if (this.Menus[index].url == 'signOut')
                this.signOut();
            else
                menu.setMainPage(this.Menus[index].url, { closeMenu: true });
        }
    }]);
})();